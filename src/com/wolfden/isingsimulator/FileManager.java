package com.wolfden.isingsimulator;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Nish - 190961209 This class is intended to act is a simple
 * centralized system for managing File I/O
 */
public class FileManager {

    private PrintWriter out;
    private FileWriter fileWriter;
    private GregorianCalendar time;

    public void createLog(String fileName, String Extension) {
        try {
            fileWriter = new FileWriter(fileName + newTime() + "." + Extension, true);
            out = new PrintWriter(fileWriter);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void writeToLog(String s) {
        out.write(s);
    }

    public String newTime() {
        time = new GregorianCalendar();
        int t1 = time.get(Calendar.SECOND);
        int t2 = time.get(Calendar.MINUTE);
        int t3 = time.get(Calendar.HOUR_OF_DAY);
        int t4 = time.get(Calendar.DAY_OF_MONTH);
        int t5 = time.get(Calendar.MONTH);
        int t6 = time.get(Calendar.YEAR);

        return String.format("%d%d%d-%d_%d_%d", t6, t5, t4, t3, t2, t1);
    }

    public void exportImageIO(int N, BufferedImage buffedImage, Component object) {

        try {
            File outputfile = new File(N + "x" + N + "_" + newTime() + ".png");
            ImageIO.write(buffedImage, "png", outputfile);
        } catch (IOException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*    public void exportImage(int N, BufferedImage bufferedImage, Component object) {
     try {
     FileOutputStream fos = new FileOutputStream(N + "x" + N + "_" + newTime() + ".jpeg");
     JPEGImageEncoder imageEncoder = JPEGCodec.createJPEGEncoder(fos);
     imageEncoder.encode(bufferedImage);
     } catch (IOException | ImageFormatException e) {
     }
     JOptionPane.showMessageDialog(object, "Exported Successfully!");
     }
     * */
}
