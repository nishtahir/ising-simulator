package com.wolfden.isingsimulator;

/**
 *
 * @author Nish - 190961209 
 * 
 * This Class is intended to serve as an object model
 * for the Lattice array list
 */
public class Cell {

    private int X;
    private int Y;
    private int Z;
    private boolean spin;

    public Cell(int x, int y, boolean spin) {
        //for 2 dimensional simulation
        setX(x);
        setY(y);
        setSpin(spin);
    }

    public Cell(int x, int y, int z, boolean spin) {
        //for 3 dminentional simulation
        throw new UnsupportedOperationException("Not yet supported.");
    }

    public int getX() {
        return X;
    }

    private void setX(int X) {
        this.X = X;
    }

    public int getY() {
        return Y;
    }

    private void setY(int Y) {
        this.Y = Y;
    }

    public int getZ() {
        return Z;
    }

    public void setZ(int Z) {
        this.Z = Z;
    }

    public boolean isSpin() {
        return spin;
    }

    public void setSpin(boolean spin) {
        this.spin = spin;
    }
}
