package com.wolfden.isingsimulator;

import javax.swing.JSlider;

/**
 *
 * @author Nish - 190961209
 * This class serves as a simple Java object to hold
 * data to make it easier to move between classes
 */
public class Data {

    public final int HOLD = 0;
    public final int HEAT = 1;
    public final int COOL = 2;
    private int J = 0;
    private int N = 0;
    private int nCount = 0;
    private int heater = 0;
    private int samples = 0;
    private double temp = 0;
    private double deltaT = 0;
    private double magnetization = 0;
    private double energy = 0;
    private javax.swing.JSlider tSlider;
    private boolean freeEnergy = false;

    public int getN() {
        return N;
    }

    public void setN(int N) {
        switch (N) {
            case 0:
                this.N = 2;
                break;
            case 1:
                this.N = 4;
                break;
            case 2:
                this.N = 6;
                break;
            case 3:
                this.N = 8;
                break;
            case 4:
                this.N = 12;
                break;
            case 5:
                this.N = 16;
                break;
            case 6:
                this.N = 32;
                break;
            case 7:
                this.N = 64;
                break;
            case 8:
                this.N = 128;
                break;
            case 9:
                this.N = 256;
                break;
        }
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setMagnetization(double Magnetization) {
        this.magnetization = Magnetization;
    }

    public double getMagnetization() {
        return magnetization;
    }

    public double getEnergy() {
        return energy;
    }

    public void setEnergy(double energy) {
        this.energy = energy;
    }

    public int getJ() {
        return J;
    }

    public void setJ(int J) {
        switch (J) {
            case 0:
                this.J = 1;
                break;
            case 1:
                this.J = -1;
                break;
        }
    }

    public int getHeater() {
        return heater;
    }

    public void setHeater(int heater) {
        this.heater = heater;
    }

    public int getnCount() {
        return nCount;
    }

    public void setnCount(int nCount) {
        this.nCount = nCount;
    }

    public JSlider gettSlider() {
        return tSlider;
    }

    public void settSlider(JSlider tSlider) {
        this.tSlider = tSlider;
    }

    public double getDeltaT() {
        return deltaT;
    }

    public void setDeltaT(double deltaT) {
        this.deltaT = deltaT;
    }

    public int getSamples() {
        return samples;
    }

    public void setSamples(int samples) {
        this.samples = samples;
    }

    public boolean isFreeEnergy() {
        return freeEnergy;
    }

    public void setFreeEnergy(boolean freeEnergy) {
        this.freeEnergy = freeEnergy;
    }
}
