package com.wolfden.isingsimulator;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JPanel;

/**
 *
 * @author Nish - 190961209
 * 
 * The idea here was to outsource as much 
 * possible to other threads to reduce the load
 * on the paintComponent in order to keep the process
 * as fast as possible
 * 
 */
public class LatticePanel extends JPanel {

    private ArrayList<ArrayList> list;
    private BufferedImage offImage;
    private Graphics2D offGraphics;
    private Cell cl;
    private Random random;
    private int J;
    private int N;
    private double magnetization = 0;
    private final double Tc = 2.26918;
    private double hamiltonian = 0;

    public LatticePanel(int N, double Temp, int J) {
        this.N = N;
        this.J = J;
        initArralList();
    }

    @Override
    public void paintComponent(Graphics g) {
        //super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        painter(g2d);
    }

    @Override
    public synchronized void update(Graphics g2d) {
        painter(g2d);
    }

    public final void initArralList() {
        list = new ArrayList<>();
        random = new Random();
        for (int i = 0; i < N; i++) {
            list.add(new ArrayList<Cell>());
        }
        //Initialize the cells with random data
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                list.get(i).add(new Cell(i, j, random.nextBoolean()));
            }
        }
    }

    public void step(double temp) {
        metropolis(temp);
        setMagnetization(magnetization());

    }

    public void stepE(double temp) {
        metropolisE(temp);
        setMagnetization(magnetization());   
    }

    public void metropolis(double temp) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cl = (Cell) list.get(i).get(j);
                double deltaE;
                deltaE = -2 * energy(cl);
                if (deltaE <= 0 || (Math.random() <= Math.exp(-deltaE / (Tc * temp)))) {
                    cl.setSpin(!cl.isSpin());
                }
            }
        }
    }

    public void metropolisE(double temp) {
        //This method includes the calculation for the free energy density
        //The population of the hamiltonian array causes a large
        //amount of computing time. Bringing about the need for a seperate 
        // method
        hamiltonian = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cl = (Cell) list.get(i).get(j);
                double deltaE;
                deltaE = -2 * energy(cl);
                if (deltaE <= 0 || (Math.random() <= Math.exp(-deltaE / (Tc * temp)))) {
                    cl.setSpin(!cl.isSpin());
                    hamiltonian = hamiltonian + energy();
                }
            }
        }
        hamiltonian = hamiltonian * (-J / (Tc * temp));
    }

    public double freeEnergyDensity() {
        //1/N^2 log(e^(-(energy)))  == energy / N^2
        return hamiltonian / (N * N);
    }

    public double magnetization() {
        int M = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cl = (Cell) list.get(i).get(j);
                if (cl.isSpin()) {
                    M++;
                } else {
                    M--;
                }
            }
        }
        return 1.0 * M / (N * N);
    }

    private void painter(Graphics g2d) {
        /*
         * Simple double buffering stategy.
         * Blast the result to the screen
         */
        if (offGraphics == null) {
            offImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            offGraphics = offImage.createGraphics();
        }
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cl = (Cell) list.get(i).get(j);
                if (cl.isSpin()) {
                    offGraphics.setColor(Color.RED);
                } else {
                    offGraphics.setColor(Color.WHITE);
                }
                offGraphics.fillRect(getWidth() / N * cl.getX(), getHeight() / N * cl.getY(),
                        getWidth() / N, getHeight() / N);
                g2d.drawImage(offImage, 0, 0, this);
                g2d.setColor(Color.BLACK);
                g2d.dispose();
            }
        }
    }

    public int energy(Cell c) {
        /*
         * Aggregate the energy by checking the
         * bonds between the lattice in all directions
         */
        int E = 0;

        if (c.isSpin() == North(c.getX(), c.getY()).isSpin()) {
            E++;
        } else {
            E--;
        }
        if (c.isSpin() == South(c.getX(), c.getY()).isSpin()) {
            E++;
        } else {
            E--;
        }
        if (c.isSpin() == East(c.getX(), c.getY()).isSpin()) {
            E++;
        } else {
            E--;
        }
        if (c.isSpin() == West(c.getX(), c.getY()).isSpin()) {
            E++;
        } else {
            E--;
        }
        return -J * E;
    }

    public double energy() {
        /*
         * Aggregate the energy
         * and halve it to mitigate
         * double counting
         */
        int E = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                cl = (Cell) list.get(i).get(j);
                E += 0.5 * energy(cl);
            }
        }
        return E;
    }

    public Cell North(int x, int y) {
        cl = (Cell) list.get(x).get(y);
        Cell cNorth = (Cell) list.get(x).get((y - 1 + N) % N);

        return cNorth;
    }

    public Cell South(int x, int y) {
        cl = (Cell) list.get(x).get(y);
        Cell cSouth = (Cell) list.get(x).get((y + 1) % N);

        return cSouth;
    }

    public Cell East(int x, int y) {
        cl = (Cell) list.get(x).get(y);
        Cell cEast = (Cell) list.get((x + 1) % N).get(y);

        return cEast;
    }

    public Cell West(int x, int y) {
        cl = (Cell) list.get(x).get(y);
        Cell cWest = (Cell) list.get((x - 1 + N) % N).get(y);

        return cWest;
    }

    public double getMagnetization() {
        return magnetization;
    }

    public void setMagnetization(double magnetization) {
        this.magnetization = magnetization;
    }

    public BufferedImage getOffImage() {
        return offImage;
    }

    public void setOffImage(BufferedImage offImage) {
        this.offImage = offImage;
    }

    public static BigDecimal bigPow(BigDecimal a, BigDecimal b) {
        BigDecimal result = a;

        if (b.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ONE;
        } else if (b.compareTo(BigDecimal.ZERO) < 0) {
            b = b.multiply(BigDecimal.valueOf(-1));
            for (BigDecimal i = BigDecimal.ZERO; i.compareTo(b.subtract(BigDecimal.ONE)) < 0; i = i.add(BigDecimal.ONE)) {
                result = BigDecimal.ONE.divide(result.multiply(a), MathContext.DECIMAL128);
            }
        } else {
            for (BigDecimal i = BigDecimal.ZERO; i.compareTo(b.subtract(BigDecimal.ONE)) < 0; i = i.add(BigDecimal.ONE)) {
                result = result.multiply(a);
            }
        }
        return result;
    }

    public static BigDecimal bigExponent(BigDecimal p) {
        BigDecimal result = new BigDecimal("0");
        BigDecimal lim = new BigDecimal("50");
        for (BigDecimal k = BigDecimal.ZERO; k.compareTo(lim) < 0; k = k.add(BigDecimal.ONE)) {
            result = result.add(bigPow(p, k).divide(bigFactorial(k), MathContext.DECIMAL128));
        }
        return result;
    }

    public static BigDecimal bigFactorial(BigDecimal n) {
        BigDecimal fact = new BigDecimal("1"); // this  will be the result
        for (BigDecimal i = BigDecimal.ONE; i.compareTo(n) <= 0; i = i.add(BigDecimal.ONE)) {
            fact = fact.multiply(i);
        }
        return fact;
    }
}
