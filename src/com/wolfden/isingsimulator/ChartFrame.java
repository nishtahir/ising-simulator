package com.wolfden.isingsimulator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 *
 * @author Nish - 190961209
 * This class serves as a simple real time monitoring tool
 *
 */
public class ChartFrame extends javax.swing.JFrame {

    private ChartPanel cp;
    FileManager file;
    private int w = 500;
    private int h = 500;

    public ChartFrame(Data d) {
        super("Magnetization");
        
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim.width - w - 300) / 2, (dim.height - h - 200) / 2);
        initComponents();
    }

    private void initComponents() {
        cp = new ChartPanel(0);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        javax.swing.GroupLayout lpLayout = new javax.swing.GroupLayout(cp);
        cp.setLayout(lpLayout);
        lpLayout.setHorizontalGroup(
                lpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 500, Short.MAX_VALUE));
        lpLayout.setVerticalGroup(
                lpLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 250, Short.MAX_VALUE));

        getContentPane().add(cp, java.awt.BorderLayout.CENTER);

        pack();
    }

    public ChartPanel getChartPanel() {
        return cp;
    }
}

class ChartPanel extends javax.swing.JPanel {

    private int MAXVALUE = 45;
    public RotatingQueue queue;
    private BufferedImage offImage;
    private Graphics2D offGraphics;

    public ChartPanel(double Magetization) {

        setBackground(Color.BLACK);
        queue = new RotatingQueue(MAXVALUE);
        for (int i = 0; i < MAXVALUE; i++) {
            queue.insertElement(0);
        }
    }

    @Override
    public void paintComponent(java.awt.Graphics g) {
        super.paintComponent(g);
        //Graphics2D g2d = (Graphics2D) g;

        g.setColor(Color.WHITE);
        painter(g);
        initChart1(g);
    }

    private void painter(Graphics g) {
        if (offGraphics == null) {
            offImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);
            offGraphics = offImage.createGraphics();
        }
        offGraphics.drawString("Magnetization", 50, 20);
        offGraphics.drawRect(5, 5, getWidth() - 10, getHeight() - 10);
        //Horizontal axis
        offGraphics.drawLine(40, 125, getWidth() - 20, 125);
        //Vertical Axis
        offGraphics.drawLine(40, 30, 40, getHeight() - 30);
        offGraphics.setColor(Color.GRAY);
        for (int i = 0; i < 45; i++) {
            if (i != 0) {
                offGraphics.drawLine(40 + (i * 10), 30, 40 + (i * 10), getHeight() - 30);
            }
        }
        for (int i = -9; i < 10; i++) {
            offGraphics.drawString(String.format("%2.1f", i / 10.0), 10, 30 + ((i + 10) * 10));
        }
        offGraphics.setColor(Color.WHITE);
        g.drawImage(offImage, 0, 0, this);
    }

    private void initChart1(Graphics g) {
        for (int i = 1; i < MAXVALUE; i++) {
            g.setColor(Color.red);
            int j = (int) queue.getElement(i);
            g.fillRect(getWidth() - 10 - (i * 10) - 3, 125 - j - 3, 6, 6);
        }

    }

    public void updateMagnetization(double m) {
        int j = (int) (m * 100);
        queue.insertElement(j);
    }
}

class RotatingQueue<T> {

    private List<T> queue;
    private int mostRecentItem;
    private int size;

    public RotatingQueue(int capacity) {
        size = capacity;
        queue = new ArrayList<>(capacity);
        mostRecentItem = capacity - 1;
    }

    /**
     * Inserts an element to the head of the queue, pushing all other elements
     * one position forward.
     *
     * @param element
     */
    public void insertElement(T element) {
        //Get index
        mostRecentItem = advancePointer(mostRecentItem);

        //Check if list already has an element
        if (queue.size() == mostRecentItem) {
            queue.add(element);
        } else {
            queue.set(mostRecentItem, element);
        }
    }

    public T getElement(int index) {
        // Normalize index to size of queue
        index = index % size;

        // Translate wanted index to queue index
        int queueIndex = mostRecentItem - index;
        // If negative, add size
        if (queueIndex < 0) {
            queueIndex += size;
        }

        // Check if element already exists in queue
        if (queueIndex < queue.size()) {
            return queue.get(queueIndex);
        } else {
            return null;
        }
    }

    public int size() {
        return size;
    }

    private int advancePointer(int oldPointer) {
        int pointer = oldPointer + 1;
        if (pointer < size) {
            return pointer;
        } else {
            return 0;
        }
    }
}