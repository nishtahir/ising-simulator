package com.wolfden.isingsimulator;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 *
 * @author Nish - 190961209
 * This class redirects an output steam to a JTextArea
 * text field
 */
public class JTextConsoleOutput extends OutputStream {

    private final JTextArea outPutField;

    public JTextConsoleOutput(JTextArea outPutField) {
        if (outPutField == null) {
            throw new IllegalArgumentException("Invalid output location");
        }
        this.outPutField = outPutField;
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        final String byteBufferInput = new String(buffer, offset, length);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                outPutField.append(byteBufferInput);
            }
        });
    }

    @Override
    public void write(int b) throws IOException {
        write(new byte[]{(byte) b}, 0, 1);
    }
}
